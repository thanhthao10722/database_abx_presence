create table employee
(
	id bigint primary key,
	name text,
	department text,
	title text,
	email text,
	phone_number bigint
);


create table room
(
	id bigint primary key,
	room_name text	
);


create table register_meeting_room
(
	id bigint primary key,
	user_id bigint references employee(id),
	room_id bigint references room(id),
	creation_date timestamp,
	register_date timestamp,
	status text,
	purpose text
);

create table manage_presence
(
	id bigint primary key,
	user_id bigint references employee(id),
	status text,
	datetime timestamp,
	image text
);


