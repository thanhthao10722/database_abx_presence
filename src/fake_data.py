import os
import pandas as pd
from faker import Faker
import datetime
import sqlalchemy


fake_data = Faker()

employee = ['id', 'name', 'department', 'title', 'email', 'phone_number']
room = ['id', 'room_name']
register_room = ['id', 'user_id', 'room_id', 'creation_date',
    'register_date', 'status', 'purpose']
manage_presence = ['id', 'user_id', 'status', 'datetime', 'image']

user_id = []
room_id = []

def fake_data_employee(oFile, table=employee):
    csv_reader = pd.DataFrame(columns=table)
    for i in range(0, 30, 1):
        employee_id = fake_data.random_number(digits = 3, fix_len = True)
        while employee_id in user_id:
           employee_id = fake_data.random_number(digits = 3, fix_len = True)
        csv_reader.at[i,'id'] = employee_id 
        user_id.append(employee_id)

        csv_reader.at[i, 'name'] = fake_data.name()
        csv_reader.at[i, 'title'] = fake_data.random_element(elements=('manager', 'employee', 'intership', 'part time'))
        csv_reader.at[i, 'department'] = fake_data.random_element(elements=('ABX', 'FSC'))
        csv_reader.at[i, 'email'] = fake_data.email()
        csv_reader.at[i, 'phone_number'] = fake_data.phone_number()
    # csv_reader.to_csv(oFile, index=False)
    return csv_reader


def fake_data_room(table=room):
    csv_reader = pd.DataFrame(columns=table)
    for i in range(0, 5, 1):
        room = fake_data.random_number(digits = 2, fix_len = True)
        while room in room_id:
           room = fake_data.random_number(digits = 2, fix_len = True)
        csv_reader.at[i,'id'] = room
        room_id.append(room)
        csv_reader.at[i, 'room_name'] = fake_data.random_element(elements=('Phòng Họp', 'Phòng Ăn', 'Phòng Làm Việc'))

    return csv_reader


def fake_data_register_room(table=register_room):
    csv_reader = pd.DataFrame(columns=table)
    ID = []
    for i in range(0, 20, 1):
        register_id = fake_data.random_number(digits = 3, fix_len = True)
        while register_id in ID:
           register_id = fake_data.random_number(digits = 3, fix_len = True)
        csv_reader.at[i,'id'] = register_id
        ID.append(register_id)
        csv_reader.at[i, 'user_id'] = fake_data.random_element(elements=user_id)
        csv_reader.at[i, 'room_id'] = fake_data.random_element(elements=room_id)

        creation_date = fake_data.date_time_between(tzinfo=None)
        csv_reader.at[i, 'creation_date'] = creation_date

        register_date = fake_data.date_time_between(end_date="now", tzinfo=None)
        while register_date < creation_date:
            register_date = fake_data.date_time_between(end_date="now", tzinfo=None)
        csv_reader.at[i, 'register_date'] = register_date
        csv_reader.at[i, 'purpose'] = fake_data.random_element(elements=('eating', 'meeting', 'others'))
        csv_reader.at[i, 'status'] = fake_data.random_element(elements=('pending' , 'approve', 'reject'))
    return csv_reader


def fake_data_manage_presence(table=manage_presence):
    csv_reader = pd.DataFrame(columns=table)
    ID = []
    for i in range(0, 20, 1):
        presence_id = fake_data.random_number(digits = 3, fix_len = True)
        while presence_id in ID:
           presence_id = fake_data.random_number(digits = 3, fix_len = True)
        csv_reader.at[i,'id'] = presence_id
        ID.append(presence_id)
        csv_reader.at[i, 'user_id'] = fake_data.random_element(elements=user_id)
        csv_reader.at[i, 'datetime'] = fake_data.date_time_between(end_date="now", tzinfo=None)       
        csv_reader.at[i, 'image'] = fake_data.file_name(extension='jpg')
        csv_reader.at[i, 'status'] = fake_data.random_element(elements=('in', 'out'))

    # csv_reader.to_csv(oFile, index=False)
    return csv_reader


def database_write(table_name, data_frame):
    engine = sqlalchemy.create_engine(
        'postgresql+psycopg2://localhost:5432/presence')
    data_frame.to_sql(name=table_name, con=engine, if_exists='replace', index=False)


if __name__  == '__main__':
    employee_data = fake_data_employee(employee)
    database_write('employee', employee_data)

    room_data = fake_data_room(room)
    database_write('room', room_data)

    register_data = fake_data_register_room(register_room)
    database_write('register_room', register_data)

    manage_data = fake_data_manage_presence(manage_presence)
    database_write('manage_presence', manage_data)

